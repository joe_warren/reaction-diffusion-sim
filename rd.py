#!/usr/bin/env python3
import numpy as np
import scipy.misc
import argparse

def laplacian(Z):
    Ztop = Z[0:-2,1:-1]
    Zleft = Z[1:-1,0:-2]
    Zbottom = Z[2:,1:-1]
    Zright = Z[1:-1,2:]
    Zcenter = Z[1:-1,1:-1]

    Ztl = Z[0:-2,0:-2]
    Ztr = Z[0:-2,2:]    
    Zbl = Z[2:, 0:-2]
    Zbr = Z[2:, 2:]

    Zcorners = Ztl + Ztr + Zbl + Zbr
    Zsides = Ztop + Zleft + Zbottom + Zright

    return ( 0.05 * Zcorners + 0.2 * Zsides - 1 * Zcenter) 

def reactionDiffusion(size, a, b, f, k, T, dt, callback):
    n = int(T/dt)
    U = np.ones((size, size))
    V = np.zeros((size, size))

    V[int(size/2-2):int(size/2+2), int(size/2-2):int(size/2+2)] = 1.0
    for i in range(n):
        # We compute the Laplacian of u and v.
        deltaU = laplacian(U)
        deltaV = laplacian(V)
        # We take the values of u and v inside the grid.
        Uc = U[1:-1,1:-1]
        Vc = V[1:-1,1:-1]
        switchover = np.multiply(Uc, np.multiply(Vc,Vc))
        # We update the variables.
        U[1:-1,1:-1], V[1:-1,1:-1] = \
            Uc + dt * (a * deltaU -switchover  +f*(1-Uc)), \
            Vc + dt * (b * deltaV + switchover -(k+f)*Vc)

        U = np.clip(U, .0, 1.)
        V = np.clip(V, .0, 1.)
        # Neumann conditions: derivatives at the edges
        # are null.
        for Z in (U, V):
            Z[0,:] = Z[1,:]
            Z[-1,:] = Z[-2,:]
            Z[:,0] = Z[:,1]
            Z[:,-1] = Z[:,-2]
        callback(U, i)

def dx(X):
    a = np.concatenate([X[-1:, :], X[0:-1, :]])
    b = np.concatenate([X[0:-1, :], X[-1:, :]])
    return (b-a)/2.0

def dy(X):
    a = X[:, 0:-2]
    b = X[:, 1:-1]
    return (b-a)/2.0

def blend(a, b , factor):
    return a* factor + b*(1.0-factor)

def toColour(U):
    def a(dx):
        if(dx < 0):
            return (0, 0, 0)
        else:
            return (-dx*10, -dx*10, -dx*10)
    def c(u):
        mix = (u-0.75)/0.1
        mix = max(0, min(mix, 1.0))
        r = blend(1., .2, mix)
        g = blend(1., .2, mix)
        b = blend(1., 1.0, mix)
        return (r, g, b)

    colours = np.array([[c(u) for u in r] for r in U]) 
    shading = np.array([[a(u) for u in r] for r in dx(U)]) 

    return np.clip(colours + shading, 0.0, 1.0)

def saveImageCB(U, i):
    if( i % 100 == 0):
        print(i)
        scipy.misc.imsave('outfile%06d.jpg'%i, toColour(U))

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('a', type=float,
                    help='Diffusion rate for substance A')
    parser.add_argument('b', type=float,
                    help='Diffusion rate for substance B')
    parser.add_argument('f', type=float,
                    help='Feed rate at which substance A is added')
    parser.add_argument('k', type=float,
                    help='Kill rate at which substance B is removed')
    parser.add_argument('--size', dest='size', type=int, default=400,
                    help='size of the simulation grid, in pixels')
    parser.add_argument('--time', dest='T', type=float, default=20000.,
                    help='simulation runtime')
    parser.add_argument('--dt', dest='dt', type=float, default=1.0,
                    help='simulation timestep')

    args = parser.parse_args()

    reactionDiffusion(args.size, args.a, args.b, args.f, args.k, args.T, args.dt, saveImageCB)
