Reaction Diffusion
==================
Simulation of two virtual chemicals reacting and diffusing on a 2d grid. Based on [This Tutorial](http://www.karlsims.com/rd.html).

Some example parameters
-----------------------
* Coral `./rd.py 1.0 0.5 0.055 0.062`
* Mitosis `./rd.py 1.0 0.5 0.0367 0.0649`
* Henna ` ./rd.py 1.0 0.5 0.035 0.062`
